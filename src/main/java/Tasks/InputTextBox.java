package Tasks;

import UI_Interface.CommonUI;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static org.openqa.selenium.Keys.ENTER;

public class InputTextBox {
    
    public static Performable inputTextBox(String inputID, String value) {
        return Task.where("{0} input the value in " + inputID,
                Enter.theValue(value).into(CommonUI.XPATH_ID.of(inputID)));
    }
    public static Performable inputTextEnter(String inputID, String value){
        return Task.where("{0} input the value in " + inputID,
                Enter.theValue(value).into(CommonUI.XPATH_ID.of(inputID)).thenHit(ENTER));

    }
}
