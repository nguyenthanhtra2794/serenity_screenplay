package Tasks;


import UI_Interface.CommonUI;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class ClickButton {
    public static Performable click(String buttonID ){
        return Task.where("{0} click button {0}", Click.on(CommonUI.XPATH_ID.of(buttonID)));
    }

}
