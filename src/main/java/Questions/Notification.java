package Questions;

import UI_Interface.CommonUI;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Text;

@Subject(" displayed Notification")
public class Notification{

       public static Question<String> value(String text){

           return  Text.of (CommonUI.TEXT_DISPLAY.of(text)).describedAs("the items displayed")
                   .asAString();

       }


}
