package UI_Interface;

import net.serenitybdd.screenplay.targets.Target;

public class CommonUI {
    public static Target XPATH_ID = Target.the("login button")
            .locatedBy("//*[@id = '{0}']");

    public static Target TEXT_DISPLAY = Target.the("text display")
            .locatedBy("//*[text() = '{0}']");
}
