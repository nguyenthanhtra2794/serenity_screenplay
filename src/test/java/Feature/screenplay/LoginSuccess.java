package Feature.screenplay;

import Questions.Notification;
import Tasks.ClickButton;
import Tasks.Application;
import Tasks.InputTextBox;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SerenityRunner.class)
public class LoginSuccess {
    private Actor landlord = Actor.named("Landlord");

    @Managed
    private WebDriver hisDriver;

    @Before
    public void landLordCanBrowseTheWeb(){

        landlord.can(BrowseTheWeb.with(hisDriver));
    }
    @Steps
    Application openTheApplication;

    @Test

    public void Test01_should_be_login_success(){
        givenThat(landlord).wasAbleTo(Application.openWeb());
        when(landlord).attemptsTo(InputTextBox.inputTextEnter("change-language","English"));
        when(landlord).attemptsTo(InputTextBox.inputTextBox("phone-login-form","388933459"));
        when(landlord).attemptsTo(InputTextBox.inputTextBox("password-login-form","404742"));


        when(landlord).attemptsTo(ClickButton.click("login-button"));
        //then(landlord).should(seeThat(Success.option(), is(Unavailable)));
        then(landlord).should(seeThat("the measage display" , Notification.value("Login success")
        ,equalTo("")));

    }

    @Test
    public void Test02_should_be_create_project_success(){
        givenThat(landlord).wasAbleTo(ClickButton.click("left-menu-projects"));
        when(landlord).attemptsTo(InputTextBox.inputTextBox("new_name","Tra test chung cu"));
        when(landlord).attemptsTo(InputTextBox.inputTextBox("new_code_name","Tra test chung cu"));
        when(landlord).attemptsTo(ClickButton.click("create_project"));
        then(landlord).should(seeThat("the measage display" ,
                Notification.value("Thêm mới dự án thành công")
                 ,equalTo("")));

        when(landlord).attemptsTo(ClickButton.click("project_building_popover_0"));
        when(landlord).attemptsTo(ClickButton.click("Vấn đề_3"));
        then(landlord).should(seeThat("the measage display" ,
                Notification.value("Vấn đề")
                ,equalTo("Vấn đề")));
    }
}
