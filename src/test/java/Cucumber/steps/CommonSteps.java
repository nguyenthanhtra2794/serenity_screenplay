package Cucumber.steps;

import Questions.Notification;
import Tasks.Application;
import Tasks.ClickButton;
import Tasks.InputTextBox;
import com.google.common.base.Splitter;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.annotations.events.AfterExample;
import net.serenitybdd.core.annotations.events.AfterScenario;
import net.serenitybdd.core.annotations.events.BeforeExample;
import net.serenitybdd.core.annotations.events.BeforeScenario;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.model.TestOutcome;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class CommonSteps {
    @Before
    public void set_the_stage() {
        setTheStage(new OnlineCast());
    }
    
    @ParameterType(".*")
    public Actor actor(String actor) {
        return OnStage.theActorCalled(actor);
    }

    @Given("{actor} open the login page")
    public void landlord_open_the_loginPage(Actor lanlord) {
        lanlord.wasAbleTo(Application.openWeb());
    }

    @When("{actor} enter dynamic {string} with value {string}")
    public void landlordEnterDynamicWithValue(Actor lanlord,String locatorID, String value) {
        lanlord.attemptsTo(InputTextBox.inputTextEnter(locatorID,value));
    }

    @And("{actor} click dynamic {string} button")
    public void landlordClickDynamicButton(Actor landlord, String buttonID) {
        when(landlord).attemptsTo(ClickButton.click(buttonID));
    }

    @Then("{actor} can see text {string}")
    public void landlordCanSeeText(Actor landlord,String exceptText) {
        then(landlord).should(seeThat("the measage display" , Notification.value(exceptText)
                ,equalTo("")));
    }



}

