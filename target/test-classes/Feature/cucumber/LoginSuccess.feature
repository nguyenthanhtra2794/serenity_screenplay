Feature: Login web admin
  I am a landlord
  I want login to the web page

  Scenario: Login success
    Given Landlord open the login page
    When Landlord enter dynamic "change-language" with value "English"
    And Landlord enter dynamic "phone-login-form" with value "388933459"
    And Landlord enter dynamic "password-login-form" with value "404742"
    And Landlord click dynamic "login-button" button
    Then Landlord can see text "Login success"

  Scenario: Create project success
    When Landlord click dynamic "left-menu-projects" button
    And Landlord enter dynamic "new_name" with value "Thanh Tra"
    And Landlord enter dynamic "new_code_name" with value "Thanh Tra1"
    And Landlord click dynamic "create_project" button
    Then Landlord can see text "Create Project success"

  Scenario: Disable issue type has level is House
    When Landlord click dynamic "create_project" button
    And  Landlord click dynamic "project_building_popover_0" button
    #Then Landlord can see text "Issue"
    When Landlord click dynamic "issue_type" button
    And Landlord click dynamic "button_issueType_edit_0" button
    And Landlord enter dynamic "react-select-2-input" with value "house"



